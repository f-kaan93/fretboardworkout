# New Version Notes

## Features

* Ask natural notes only or all notes
* If all notes select flat or sharp notes
* Ask selection of notes
* Ask only on selection of strings
* Show or hide natural note places on keyboard on view
* 24/12 fret view
* Show or hide empty string notes

