var game = new Phaser.Game(800, 700, Phaser.AUTO, 'game');

var fretboard,
    marker,
    position,
    buttons,
    selection,
    activeNote,
    correct,
    incorrect,
    naturalNotes = [
        [[1, 1, "F"], [1, 3, "G"], [1, 5, "A"], [1, 7, "B"], [1, 8, "C"], [1, 10, "D"], [1, 12, "E"]],
        [[2, 1, "C"], [2, 3, "D"], [2, 5, "E"], [2, 6, "F"], [2, 8, "G"], [2, 10, "A"], [2, 12, "B"]],
        [[3, 2, "A"], [3, 4, "B"], [3, 5, "C"], [3, 7, "D"], [3, 9, "E"], [3, 10, "F"], [3, 12, "G"]],
        [[4, 2, "E"], [4, 3, "F"], [4, 5, "G"], [4, 7, "A"], [4, 9, "B"], [4, 10, "C"], [4, 12, "D"]],
        [[5, 2, "B"], [5, 3, "C"], [5, 5, "D"], [5, 7, "E"], [5, 8, "F"], [5, 10, "G"], [5, 12, "A"]],
        [[6, 1, "F"], [6, 3, "G"], [6, 5, "A"], [6, 7, "B"], [6, 8, "C"], [6, 10, "D"], [6, 12, "E"]],
    ];


var Main = {
    preload: function () {
        game.load.image('Fretboard', 'assets/Fretboard.png');
        game.load.image('Marker', 'assets/Marker.png');
        game.load.image('Correct', 'assets/Correct.png');
        game.load.image('Incorrect', 'assets/Incorrect.png');
        game.load.image('A', 'assets/A.png');
        game.load.image('B', 'assets/B.png');
        game.load.image('C', 'assets/C.png');
        game.load.image('D', 'assets/D.png');
        game.load.image('E', 'assets/E.png');
        game.load.image('F', 'assets/F.png');
        game.load.image('G', 'assets/G.png');
    },
    create: function () {
        game.stage.backgroundColor = '#ffffff';
        fretboard = game.add.image(game.world.centerX, game.world.centerY, 'Fretboard');
        fretboard.anchor.set(0.5, 0.5);

        correct = game.add.image(game.world.centerX, game.world.height - 190, 'Correct');
        correct.anchor.set(0.5, 0.5);
        correct.alpha = 0;

        incorrect = game.add.image(game.world.centerX, game.world.height - 190, 'Incorrect');
        incorrect.anchor.set(0.5, 0.5);
        incorrect.alpha = 0;
        marker = game.add.image(-10, -10, 'Marker');
        marker.anchor.set(0.5, 0.5);
        buttons = {
            A: game.add.button((game.world.centerX + 50 * -3), game.world.height - 100, "A", function () {
                selection = "A";
                Main.checkAnswer();
            }),
            B: game.add.button((game.world.centerX + 50 * -2), game.world.height - 100, "B", function () {
                selection = "B";
                Main.checkAnswer();
            }),
            C: game.add.button((game.world.centerX + 50 * -1), game.world.height - 100, "C", function () {
                selection = "C";
                Main.checkAnswer();
            }),
            D: game.add.button((game.world.centerX + 50 * 0), game.world.height - 100, "D", function () {
                selection = "D";
                Main.checkAnswer();
            }),
            E: game.add.button((game.world.centerX + 50 * 1), game.world.height - 100, "E", function () {
                selection = "E";
                Main.checkAnswer();
            }),
            F: game.add.button((game.world.centerX + 50 * 2), game.world.height - 100, "F", function () {
                selection = "F";
                Main.checkAnswer();
            }),
            G: game.add.button((game.world.centerX + 50 * 3), game.world.height - 100, "G", function () {
                selection = "G";
                Main.checkAnswer();
            })

        }
        this.randomize();


    },
    update: function () {

    },
    seeAllNotes: function () {
        naturalNotes.forEach(function (note) {
            var coor = Main.getCoordination(note)
            var i = game.add.image(coor.x, coor.y, 'Marker');
            i.anchor.set(0.5, 0.5);
            var text = game.add.text(i.x, i.y + 2, note[2], {
                font: '12px Arial',
                fill: '#f5f5f5',
            });
            text.anchor.set(0.5, 0.5);

        })
    },
    getCoordination: function (note) {
        var obj = {
            x: fretboard.left + 29 + (54 * (note[1] - 1)),
            y: fretboard.top + (24 * (note[0] - 1)),
            note: note[2]
        }
        return obj;

    },
    randomize: function () {
        activeNote = naturalNotes[game.rnd.integerInRange(0, 5)][game.rnd.integerInRange(0, 6)];
        marker.x = this.getCoordination(activeNote).x;
        marker.y = this.getCoordination(activeNote).y;
    },
    checkAnswer: function () {
        if (selection === activeNote[2]) {
            game.add.tween(correct).to({ alpha: 1 }, 100, Phaser.Easing.Default)
                .to({ alpha: 1 }, 300, Phaser.Easing.Default)
                .to({ alpha: 0 }, 100, Phaser.Easing.Default, true);
            selection = null;
            this.randomize();
        } else {
            game.add.tween(incorrect).to({ alpha: 1 }, 100, Phaser.Easing.Default)
                .to({ alpha: 1 }, 300, Phaser.Easing.Default)
                .to({ alpha: 0 }, 100, Phaser.Easing.Default, true);
        }

    }
}

game.state.add('Main', Main);
game.state.start('Main');